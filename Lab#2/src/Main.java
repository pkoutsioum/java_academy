import model.Company;
import services.InitEmployee;

public class Main {

    public static void main(String[] args) {
        Company company = new Company();
        company.simpleStatistics(InitEmployee.listEmployee());
    }
}
