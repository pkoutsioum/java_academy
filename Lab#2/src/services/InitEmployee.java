package services;

import model.Employee;

import java.util.ArrayList;
import java.util.List;

public class InitEmployee {
    public static List<Employee> listEmployee() {

        List<Employee> employeeList = new ArrayList<Employee>();

        employeeList.add(new Employee("Panos Kou", 1504, "Developer", "ekei 43"));
        employeeList.add(new Employee("Mitsos AK", 905, "Developer", "edwbbb 54"));
        employeeList.add(new Employee("Giannis Krat", 1604, "Developer", "pio dei3ia 23"));
        employeeList.add(new Employee("Koula Kaaaa", 1704, "Manager", "pio aristera 36"));

        return employeeList;
    }

}