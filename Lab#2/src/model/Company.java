package model;

import java.util.List;
import java.util.Scanner;

public class Company {

    public Company() {
    }

    public void simpleStatistics(List<Employee> employeeList){
        double sum = 0;
        double yearsum = 0;
        int id = 0;
        int choice = 0;

        System.out.println("Give your choice. \n\r1: Print" + "\n\r2: Exit");
        Scanner scanner = new Scanner(System.in);
        choice = scanner.nextInt();
        if (choice == 1 || choice == 2) {
            switch (choice) {
                case 1:
                    for (Employee emp : employeeList) {
                        id++;
                        emp.calc();
                        System.out.println("id is: " + id + " Employee Name : " + emp.getFullname() + " " + emp.getType() + " " + emp.getSalary() + " " + emp.getAddress());
                        sum += emp.getSalary();
                    }

                    yearsum = 12 * sum;
                    System.out.println("Year Salary is : " + yearsum);
                    break;
                case 2:
                    System.out.println("Exit");
                    break;
                default:
                    System.out.println("no action");
                    break;
            }
        }
        else {
            System.out.println("Give 1 or 2");
            return;
        }
    }
}
