import javax.swing.*;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.time.LocalDateTime;

public class CalcStats extends HeartRates {

    public void calculate(){
        int age = 0;
        int hrmax = 0;
        int hrtar = 0;
        int intyear = 0;

        String fname = JOptionPane.showInputDialog("Please enter first name:");
        setFirstName(fname);

        String lname = JOptionPane.showInputDialog("Please enter last name:");
        setLastName(lname);

        String bday = JOptionPane.showInputDialog("Please enter day of birth:");
        setDay(bday);

        String bmonth = JOptionPane.showInputDialog("Please enter month of birth:");
        setMonth(bmonth);

        String byear = JOptionPane.showInputDialog("Please enter year of birth");
        setYear(byear);

        LocalDateTime now = LocalDateTime.now();
        intyear = Integer.parseInt(byear);
        age = now.getYear() - intyear;
        hrmax = 220 - age;
        hrtar = (85*hrmax)/100;

        PrintWriter pw;
        try{
            pw = new PrintWriter("HeartRateStats.txt");
            pw.println("First Name: "+getFirstName()
                    +"\n\rLast Name: "+ getLastName() +
                    "\n\rDay of Birth: "+ getDay() +
                    "\n\rMonth of birth: "+ getMonth() +
                    "\n\rYear of Birth "+ getYear() +
                    "\n\rAge is: "+ age +
                    "\n\rMax HeartRate is: "+ hrmax +
                    "\n\rTarget HeartRate is: "+ hrtar);
            pw.println("End of file");
            pw.close();
        } catch (FileNotFoundException e){
            System.out.println("Error Occured!");
        }

        JOptionPane.showMessageDialog(null, "First Name: "+getFirstName()
                +" Last Name: "+ getLastName() +
                " Day of Birth: "+ getDay() +
                " Month of birth: "+ getMonth() +
                " Year of Birth "+ getYear() +
                " Age is: "+ age +
                " Max HeartRate is: "+ hrmax +
                " Target HeartRate is: "+ hrtar);
    }
}
