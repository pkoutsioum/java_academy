package main;

import model.Employee;
import services.FileService;
import services.InitEmployees;

import java.util.List;

public class Main_txt {

    public static void main(String[] args) {
        FileService fileService = new FileService();
        fileService.writeToTxtFile(InitEmployees.listEmployee());
        fileService.readFromTxtFile();
    }
}
