package model;

import java.util.ArrayList;

public class Company {
    private String name;
    private String address;
    private ArrayList<Employee> employees;

    public Company(String name, String address) {
        this.name = name;
        this.address = address;
        employees = new ArrayList();
    }
}
