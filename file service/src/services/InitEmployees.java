package services;

import model.Employee;

import java.util.ArrayList;
import java.util.List;
public class InitEmployees {

    public static List<Employee> listEmployee() {

        List<Employee> employeeList = new ArrayList<Employee>();

        Employee employee1 = new Employee("Panagiotis", "Tsarou 52");
        Employee employee2 = new Employee("Kostas", "Taurou 18");
        Employee employee3 = new Employee("Thomas", "Selou 45");
        Employee employee4 = new Employee("Nikos", "Malou 25");

        employeeList.add(employee1);
        employeeList.add(employee2);
        employeeList.add(employee3);
        employeeList.add(employee4);

        return employeeList;
    }
}