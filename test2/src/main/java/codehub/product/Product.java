package codehub.product;

import lombok.Data;

@Data
public class Product extends AbstractProduct {
    private String provider;
    private String size;
}
