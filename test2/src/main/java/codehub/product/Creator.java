package codehub.product;

import java.util.List;

public class Creator {
    public AbstractProduct productionFactoryMethod(TypeOfProduct typeOfProduct){
        switch (typeOfProduct){
            case PRODUCT:
                return new Product();
            case SERVICE:
                return new Service();
            case SUBSCRIPTION:
                return new Subscription();
            default:
                return null;
        }
    }
}
