package codehub.product;

public enum TypeOfProduct {
    PRODUCT, SERVICE, SUBSCRIPTION;
}
