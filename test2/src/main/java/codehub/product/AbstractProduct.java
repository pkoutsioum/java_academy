package codehub.product;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Data

public class AbstractProduct {
    private int id;
    private String name;
    private double price;
}
