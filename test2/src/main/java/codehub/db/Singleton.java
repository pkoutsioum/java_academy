package codehub.db;

public class Singleton {
    private Singleton(){
    }
    private static Singleton _instance = null;
    public static Singleton getInstance() {
        if (_instance == null) _instance = new Singleton();
        return _instance;
    }
    public void act(){
        System.out.println("Hello from Singleton!");
    }
}
