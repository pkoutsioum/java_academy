import java.util.LinkedList;
import java.util.List;

import static java.lang.System.exit;

public class Main {

    public static void main(String[] args) {
        List<String> s1 = new LinkedList<String>();
        List x = s1;
        x.add(new Integer(5));
        try{
            String str = s1.get(0);
            System.out.println(str);
        }
        catch (Exception e){
            System.out.println("Error! Cause: \n\r"+e.toString());
            exit(0);
        }
    }
}
